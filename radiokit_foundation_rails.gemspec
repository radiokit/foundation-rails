$:.push File.expand_path("../lib", __FILE__)

require "radiokit_foundation_rails/version"

Gem::Specification.new do |s|
  s.name        = "radiokit_foundation_rails"
  s.version     = RadioKit::Foundation::Rails::VERSION
  s.authors     = ["Marcin Lewandowski"]
  s.email       = ["marcin@radiokit.org"]
  s.homepage    = "http://radiokit.org"
  s.summary     = "Foundation to build Rails apps that are part of the RadioKit ecosystem"
  s.description = "RadioKit Foundation allows you to use common patterns found in the RadioKit ecosystem"

  s.add_dependency "rails", "~> 4.2.1"
  s.add_dependency 'jbuilder', '~> 2.0'
  s.add_dependency 'oj'
  s.add_dependency 'multi_json'
  s.add_dependency 'pundit', '~> 1.0'
  s.add_dependency 'faraday'
  s.add_dependency 'newrelic_rpm'
  s.add_dependency 'lograge'
  s.add_dependency 'rack-cors'
  s.add_dependency 'faye-websocket'
  s.add_dependency 'redis'
  s.add_dependency 'hiredis'

  s.files         = `git ls-files`.split("\n")
  s.require_paths = ['lib']
  s.required_ruby_version = '>= 1.9.3'
end