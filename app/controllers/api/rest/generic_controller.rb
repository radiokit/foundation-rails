class Api::Rest::GenericController < ActionController::Base
  include RadioKit::Foundation::Rails::RestApi::Inbound::Controller

  skip_before_action :initialize_common_variables
  before_action :initialize_common_variables_generic


  private

  def initialize_common_variables_generic
    @model_name             = params[:model].classify
    @model                  = @model_name.constantize
    @variable_params        = :"#{params[:model]}"
    @variable_singular_name = "@#{params[:model].gsub("/", "_")}"
    @variable_plural_name   = "@#{params[:model].gsub("/", "_").pluralize}"
  end
end