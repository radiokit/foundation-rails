json.set! :data do
  if params.has_key? :j
    json.array! instance_variable_get(@variable_plural_name) do |record|
      json.(record, *(@attrs))

      params[:j].uniq.each do |join_param|
        if @model.reflections[join_param.to_s].is_a? ActiveRecord::Reflection::BelongsToReflection
          json.set! join_param do
            json.(record.send(join_param), *(@model.reflections[join_param.to_s].klass.const_get(:API_INDEX_ATTRS)))
          end
        else
          json.set! join_param do
            json.array!(record.send(join_param), *(@model.reflections[join_param.to_s].klass.const_get(:API_INDEX_ATTRS)))
          end
        end
      end
    end

  else
    json.array! instance_variable_get(@variable_plural_name), *(@attrs)
  end
end

if @count_total
  json.set! :meta do
    json.count_total @count_total
  end
else
  json.set! :meta do
  end
end
