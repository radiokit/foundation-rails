json.set! :data do
  if params.has_key? :j
    json.(instance_variable_get(@variable_singular_name), *(@attrs))

    params[:j].uniq.each do |join_param|
      if @model.reflections[join_param.to_s].is_a? ActiveRecord::Reflection::BelongsToReflection
        json.set! join_param do 
          json.(instance_variable_get(@variable_singular_name).send(join_param), *(@model.reflections[join_param.to_s].klass.const_get(:API_INDEX_ATTRS)))
        end
      else
        json.set! join_param do 
          json.array!(instance_variable_get(@variable_singular_name).send(join_param), *(@model.reflections[join_param.to_s].klass.const_get(:API_INDEX_ATTRS)))
        end
      end
    end
  else
    json.(instance_variable_get(@variable_singular_name), *(@attrs))
  end
end