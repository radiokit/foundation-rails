class UriValidator < ActiveModel::EachValidator
  DEFAULT_SCHEMES = [ "http", "https" ].freeze
  DISALLOWED_HOSTNAMES = [ "localhost", "ip6-localhost", "ip6-loopback", "ip6-localnet", "ip6-mcastprefix", "ip6-allnodes", "ip6-allrouters" ].freeze

  def validate_each(record, attribute, value)
    @break = false

    validate_has_valid_syntax(record, attribute, value)
    validate_has_allowed_scheme(record, attribute, value)
    validate_does_not_contain_blocked_hostname(record, attribute, value) unless @break
    validate_does_not_contain_my_hostname(record, attribute, value) unless @break
    validate_does_not_contain_local_hostname(record, attribute, value) unless @break
    validate_does_not_contain_private_ip(record, attribute, value) unless @break
  end


  def validate_has_valid_syntax(record, attribute, value)
    return if value.blank?

    URI.parse(value)

  rescue URI::InvalidURIError => e
    record.errors[attribute] << "has invalid syntax"
  end


  def validate_has_allowed_scheme(record, attribute, value)
    return if value.blank?

    allowed_schemes = options[:allowed_schemes] || DEFAULT_SCHEMES

    unless allowed_schemes.include? URI.parse(value).scheme
      record.errors[attribute] << "contains disallowed scheme"
      @break = true
    end 

  rescue URI::InvalidURIError => e
    # Do nothing, previous validation should handle this
  end


  def validate_does_not_contain_blocked_hostname(record, attribute, value)
    return if value.blank?

    disallowed_hostnames = options[:disallowed_hostnames] || DISALLOWED_HOSTNAMES

    record.errors[attribute] << "contains disallowed hostname" if disallowed_hostnames.include? URI.parse(value).host

  rescue URI::InvalidURIError => e
    # Do nothing, previous validation should handle this
  end


  def validate_does_not_contain_my_hostname(record, attribute, value)
    return if value.blank?

    record.errors[attribute] << "contains disallowed hostname" if Socket.gethostname == URI.parse(value).host

  rescue URI::InvalidURIError => e
    # Do nothing, previous validation should handle this
  end


  def validate_does_not_contain_local_hostname(record, attribute, value)
    return if value.blank?

    host = URI.parse(value).host

    if host
      record.errors[attribute] << "contains disallowed hostname" if host.ends_with? ".local"
    end

  rescue URI::InvalidURIError => e
    # Do nothing, previous validation should handle this
  end


  def validate_does_not_contain_private_ip(record, attribute, value)
    return if value.blank?

    ip = URI.parse(value).host
    addr = IPAddr.new(ip)

    if IPAddr.new("10.0.0.0/8").include?(ip) or
       IPAddr.new("172.16.0.0/12").include?(ip) or
       IPAddr.new("192.168.0.0/16").include?(ip) or
       IPAddr.new("100.64.0.0/10").include?(ip) or
       IPAddr.new("127.0.0.0/8").include?(ip) or
       IPAddr.new("fd00::/8").include?(ip) or
       IPAddr.new("fe80::/10").include?(ip) or
       IPAddr.new("::1").include?(ip)


      record.errors[attribute] << "contains disallowed IP address" 
    end

  rescue IPAddr::InvalidAddressError => e
    # Do nothing, location does not point to the IP address

  rescue URI::InvalidURIError => e
    # Do nothing, previous validation should handle this
  end
end


