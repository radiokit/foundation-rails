module RadioKit
  module Foundation
    module Rails
      module GenericModel
        extend ActiveSupport::Concern

        included do
          DEFAULT_ALIAS_LENGTH = 16
          DEFAULT_ALIAS_CHARS  = ((0..9).to_a + ('a'..'z').to_a + ('A'..'Z').to_a).freeze

          before_validation :set_default_alias_if_not_specified, on: :create

          validates :domain, presence: true, format: { with: /\A[a-zA-Z0-9\-\.\_]+\z/ }
          validates :alias, presence: true, uniqueness: { scope: :domain }, format: { with: /\A[a-zA-Z0-9\-\.\_]+\z/ }

          private

          def set_default_alias_if_not_specified
            if self.alias.blank?
              loop do
                current = Array.new(DEFAULT_ALIAS_LENGTH) { DEFAULT_ALIAS_CHARS.to_a[rand(DEFAULT_ALIAS_CHARS.to_a.size)] }.join

                unless self.class.exists?(alias: current)
                  self.alias = current
                  break
                end
              end
            end
          end
        end
      end
    end
  end
end

