# Usage

Initialize a new Rails project:

    rails new project
    cd project/

Put this into Gemfile

```` ruby
ruby '2.2.1'
source 'https://rubygems.org'

gem 'rails', '4.2.1'

gem "psych", "~> 2.0.5"                                   # Force upgrade of underlying libyaml to >= 0.1.6 to overcome CVE-2014-2525
gem 'pg'                                                  # For access to PostgreSQL
gem 'rack-timeout'                                        # For timeouting requests (puma does not do this on its own)
gem 'radiokit_foundation_rails', git: "https://radiokitrepo:pamparam@bitbucket.org/radiokit/foundation-rails.git", branch: "stable"

group :development do
  gem 'quiet_assets'                                      # To not spam console by GETs that are related to assets
  gem 'spring'                                            # For keeping Rails instances alive to save time on reloads
  gem 'foreman'                                           # For running apps
  gem 'pry-rails'                                         # For nice console
end

group :development, :test do
  gem 'spring-commands-rspec'                             # For using RSpec along with spring
  gem 'rspec-rails', '~> 3.1.0'                           # For writing automated tests suite
  gem 'factory_girl_rails'
  gem 'faker'
end

group :production do
  gem 'rails_12factor'
  gem 'puma'                                              # For super-fast webserver
end
````

Bundle it

    bundle install

Update database configuration, put this into `config/database.yml`:

    development:
      adapter: postgresql
      username: radiokit-APPNAME-dev
      database: radiokit-APPNAME-dev
      password: radiokit-APPNAME-dev
      encoding: utf8
      host: localhost

    test:
      adapter: postgresql
      username: radiokit-APPNAME-test
      database: radiokit-APPNAME-test
      password: radiokit-APPNAME-test
      encoding: utf8
      host: localhost

    staging:
      url:  <%= ENV["DATABASE_URL"] %>
      pool: <%= ENV["DB_POOL"] || ENV['MAX_THREADS'] || 5 %>

    production:
      url:  <%= ENV["DATABASE_URL"] %>
      pool: <%= ENV["DB_POOL"] || ENV['MAX_THREADS'] || 5 %>

Create appropriate databases:

    createuser -W radiokit-APPNAME-dev
    createdb -O radiokit-APPNAME-dev radiokit-APPNAME-dev
    createuser -W radiokit-APPNAME-test
    createdb -O radiokit-APPNAME-test radiokit-APPNAME-test


Add staging environment to `config/secrets.yml`:

    staging:
      secret_key_base: <%= ENV["SECRET_KEY_BASE"] %>

Add staging environment to `config/environments/staging.rb:

``` ruby
Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Enable Rack::Cache to put a simple HTTP cache in front of your application
  # Add `rack-cache` to your Gemfile before enabling this.
  # For large-scale production use, consider using a caching reverse proxy like
  # NGINX, varnish or squid.
  # config.action_dispatch.rack_cache = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.serve_static_files = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = :uglifier
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # `config.assets.precompile` and `config.assets.version` have moved to config/initializers/assets.rb

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups.
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address:              ENV["SMTP_HOST"],
    port:                 ENV["SMTP_PORT"].to_i,
    user_name:            ENV["SMTP_USERNAME"],
    password:             ENV["SMTP_PASSWORD"],
    enable_starttls_auto: true  
  }
  config.action_mailer.default_url_options = { :host => ENV["PUBLIC_HOST"], :port => ENV["PUBLIC_PORT"].to_i, :protocol => ENV["PUBLIC_PROTOCOL"] }
end
```

Set connection timeout, create `config/initializers/timeout.rb` with the following content:

``` ruby
Rack::Timeout.timeout = 10  # seconds
```

Disable assets, set timezone etc. in `config/application.rb`:

``` ruby
config.time_zone = 'UTC'

config.assets.enabled = false

config.active_record.raise_in_transactional_callbacks = true

# FIXME why it does not work when called from foundation?
config.lograge.enabled = true
config.logger = Logger.new(STDOUT) if Rails.env.staging? or Rails.env.production?
```

Configure puma webserver, put this into `config/puma.rb:

``` ruby
workers Integer(ENV['WEB_CONCURRENCY'] || 2)
threads_count = Integer(ENV['MAX_THREADS'] || 5)
threads threads_count, threads_count

preload_app!

rackup      DefaultRackup
port        ENV['PORT']     || 3000
environment ENV['RACK_ENV'] || 'development'

on_worker_boot do
  # Worker specific setup for Rails 4.1+
  # See: https://devcenter.heroku.com/articles/deploying-rails-applications-with-the-puma-web-server#on-worker-boot
  ActiveRecord::Base.establish_connection
end
```

Configure Procfile, put this into `Procfile`:

```
web: bundle exec puma -C config/puma.rb
```

Integrate with shippable, put this into `shippable.yml`:

``` yaml
language: ruby

env:
  global:
    - secure: YkCYxCSPex+pKLMTGnP8Tn9iqogV3KNgrIv4wZg0NscR0lt3KqziB6z3x8inhvyycj6kkYlZT1rnWpV/owmpSR9LAijXVjHLTf04uy9PFAjZkWBwsJu2kFNWQvESO7C6sZmx6lZXxDuCiENrb35hIHZ+cDJIegutEFVmBSARCnLzT54FsaERYLF/oxcsefvbUfRFkFG/wMztIdJVzX3ZbjSvDTyWm88W75J6TKfFq+1+hJAiCSOQW5vlTcM+VFT+UUEeiDlnJI2W8hVo2HuWxLQegBSdgJ9xNV63hqo2uU2yMobUALssurqcqTnvBUDZ1wDv6mCilL9R4BDXHeuerQ==

after_failure:
  - sudo apt-get install wget -y; wget -O /tmp/notify.py http://mspanc.github.io/shippable-slack-notifier/notify.py; python /tmp/notify.py --slack-url $SLACK_URL --message failure
after_success:
  - sudo apt-get install wget -y; wget -O /tmp/notify.py http://mspanc.github.io/shippable-slack-notifier/notify.py; python /tmp/notify.py --slack-url $SLACK_URL --message success

rvm:
  - 2.2.2

build_image: shippableimages/ubuntu1404_ruby

before_install:
  - source ~/.rvm/scripts/rvm
  - rvm install $SHIPPABLE_RUBY --verify-downloads 1
  - source ~/.bashrc && ~/.rvm/scripts/rvm && rvm use $SHIPPABLE_RUBY
  - sudo apt-get update
  - sudo DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes redis-server postgresql libpq-dev nodejs
  - sudo echo -e "local all postgres trust\nhost all postgres 127.0.0.1/32 trust\nhost all postgres ::1/128 trust" > /etc/postgresql/9.3/main/pg_hba.conf
  - sudo /etc/init.d/postgresql start
  - sudo /etc/init.d/redis-server start

before_script:
  - psql -c 'create database radiokit_test;' -U postgres
  - DATABASE_URL=postgresql://postgres:@localhost/radiokit_test?pool=5 bundle install
  - DATABASE_URL=postgresql://postgres:@localhost/radiokit_test?pool=5 bundle exec rake db:migrate

install:
  - bundle install --gemfile="Gemfile"
  - ruby -v

script:
  - DATABASE_URL=postgresql://postgres:@localhost/radiokit_test?pool=5 bundle exec rspec spec/
```

Remove Test::Unit and initialize RSpec

```
rm -rf test/
rails g rspec:install
```

