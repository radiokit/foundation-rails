module RadioKit
  module Foundation
    module Rails
      module InternalApi
        class InternalApiConfigurationError < RuntimeError; end
        class InternalApiInvalidCallError < RuntimeError; end


        def self.app_name_to_env_name(app_name)
          app_name.to_s.upcase.gsub("-", "_")
        end
      end
    end
  end
end