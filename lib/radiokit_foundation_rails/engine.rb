module RadioKit
  module Foundation
    module Rails
      class Engine < ::Rails::Engine
        isolate_namespace RadioKit::Foundation::Rails

        initializer "radiokit_foundation_rails.lograge" do |app|
          app.config.lograge.enabled = true
          app.config.logger = ::Logger.new(STDOUT) if ::Rails.env.staging? or ::Rails.env.production?
        end


        initializer "radiokit_foundation_rails.stream_api" do |app|
          app.config.middleware.insert_before 0, "RadioKit::Foundation::Rails::StreamApi::Middleware"
        end

        initializer "radiokit_foundation_rails.rest_api.cors" do |app|
          app.config.middleware.insert_before 0, "Rack::Cors" do
            allow do
              origins '*'
              resource '/api/rest/*', headers: :any, methods: [:get, :post, :patch, :delete, :options]

            end
          end
        end
      end
    end
  end
end
