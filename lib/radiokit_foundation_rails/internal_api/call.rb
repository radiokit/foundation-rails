module RadioKit
  module Foundation
    module Rails
      module InternalApi
        class Call
          def self.get(app_name, location, params = {})
            request :get, app_name, location, params
          end


          def self.put(app_name, location, params = {})
            request :put, app_name, location, params
          end


          def self.post(app_name, location, params = {})
            request :post, app_name, location, params
          end


          def self.patch(app_name, location, params = {})
            request :patch, app_name, location, params
          end


          def self.delete(app_name, location, params = {})
            request :delete, app_name, location, params
          end


          private

          def self.request(method, app_name, location, params = {})
            raise InternalApiConfigurationError, "Unable to make internal API call to the app '#{app_name}', location: #{method.to_s.upcase} #{location}, params: #{params.inspect}: Please set environment variable SELF_NAME" unless ENV.has_key? "SELF_NAME"

            target_env_name = InternalApi.app_name_to_env_name(app_name)
            self_env_name = InternalApi.app_name_to_env_name(ENV["SELF_NAME"])

            raise InternalApiConfigurationError, "Unable to make internal API call to the app '#{app_name}', location: #{method.to_s.upcase} #{location}, params: #{params.inspect}: Please set environment variable APP_#{target_env_name}_BASE_URL" unless ENV.has_key? "APP_#{target_env_name}_BASE_URL"
            raise InternalApiConfigurationError, "Unable to make internal API call to the app '#{app_name}', location: #{method.to_s.upcase} #{location}, params: #{params.inspect}: Please set environment variable APP_#{self_env_name}_INTERNAL_API_KEY" unless ENV.has_key? "APP_#{self_env_name}_INTERNAL_API_KEY"
            
            raise InternalApiInvalidCallError, "Unable to make internal API call to the app '#{app_name}', location: #{method.to_s.upcase} #{location}, params: #{params.inspect}: Location must start with '/'" unless location[0] == "/"
            raise InternalApiInvalidCallError, "Unable to make internal API call to the app '#{app_name}', location: #{method.to_s.upcase} #{location}, params: #{params.inspect}: Params must be a hash" unless params.is_a? Hash
            raise InternalApiInvalidCallError, "Unable to make internal API call to the app '#{app_name}', location: #{method.to_s.upcase} #{location}, params: #{params.inspect}: Location should not not have prefix of '/api/internal' - use only part of the location that is specific to the call" if location.starts_with? "/api/internal"

            Logger.info "[InternalApi::Call] #{method.to_s.upcase} to #{app_name} #{location} #{params.inspect}" 
            conn = ::Faraday.new ENV["APP_#{target_env_name}_BASE_URL"]
            conn.basic_auth ENV["SELF_NAME"], ENV["APP_#{self_env_name}_INTERNAL_API_KEY"] 
            conn.params = params
            conn.send method, "/api/internal#{location}"
          end
        end
      end
    end
  end
end
