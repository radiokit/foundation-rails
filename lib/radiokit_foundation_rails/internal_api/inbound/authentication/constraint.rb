module RadioKit
  module Foundation
    module Rails
      module InternalApi
        module Inbound
          module Authentication
            class Constraint
              def matches?(request)
                Authentication.authenticated? request
              end
            end
          end
        end
      end
    end
  end
end