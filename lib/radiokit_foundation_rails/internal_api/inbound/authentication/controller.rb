require 'active_support/concern'

module RadioKit
  module Foundation
    module Rails
      module InternalApi
        module Inbound
          module Authentication
            module Controller
              extend ActiveSupport::Concern

              included do
                before_action :authenticate_inbound_internal_api_call!

                private

                def authenticate_inbound_internal_api_call!
                  authenticate_or_request_with_http_basic do |username, password|
                    Authentication.authenticated? request
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
