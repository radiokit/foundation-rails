module ActionDispatch::Routing
  class Mapper
    def radiokit_internal_api(&b)
      namespace :api do
        namespace :internal do
          constraints ::RadioKit::Foundation::Rails::InternalApi::Inbound::Authentication::Constraint.new do
            yield
          end
        end
      end
    end
  end
end

