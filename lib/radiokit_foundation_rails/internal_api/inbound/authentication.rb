module RadioKit
  module Foundation
    module Rails
      module InternalApi
        module Inbound
          module Authentication
            def self.authenticated?(request)
              if ::Rails.env.development? or ::Rails.env.test?
                Logger.warn "[InternalApi::Inbound::Authentication #{request.method} #{request.path}] Skipping authentication in development/test environment"

              else
                return false unless request.headers["Authorization"]
                return false unless request.headers["Authorization"].starts_with? "Basic "

                username, password = Base64.decode64(request.headers["Authorization"].split(" ", 2).last).split(":")

                env_name = InternalApi.app_name_to_env_name(username) 
                
                raise InternalApiConfigurationError, "Unable to authenticate inbound internal API call, please set environment variable 'APP_#{env_name}_INTERNAL_API_KEY'" unless ENV.has_key? "APP_#{env_name}_INTERNAL_API_KEY"  

                if ENV.has_key? "APP_#{env_name}_INTERNAL_API_HOSTS"
                  if ENV["APP_#{env_name}_INTERNAL_API_HOSTS"].split(" ").include?(request.remote_ip)
                    if ENV["APP_#{env_name}_INTERNAL_API_KEY"] == password
                      Logger.info "[InternalApi::Inbound::Authentication #{request.method} #{request.path}] Authentication ok: authenticated app #{username}"
                      true
                    
                    else
                      Logger.warn "[InternalApi::Inbound::Authentication #{request.method} #{request.path}] Authentication failed: invalid password for app #{username}"
                      false
                    end
                  
                  else
                    Logger.warn "[InternalApi::Inbound::Authentication #{request.method} #{request.path}] Authentication failed: #{request.remote_ip} is disallowed for app #{username}, allowed hosts are #{ENV["APP_#{env_name}_INTERNAL_API_HOSTS"]}"
                    false
                  end
    
                else
                  if ENV["APP_#{env_name}_INTERNAL_API_KEY"] == password
                    Logger.info "[InternalApi::Inbound::Authentication #{request.method} #{request.path}] Authentication ok: authenticated app #{username}"
                    true
                  
                  else
                    Logger.warn "[InternalApi::Inbound::Authentication #{request.method} #{request.path}] Authentication failed: invalid password for app #{username}"
                    false
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
