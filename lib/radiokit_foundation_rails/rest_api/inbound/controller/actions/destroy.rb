

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Actions
              module Destroy
                def self.included(base)
                
                  base.class_eval do
                    def destroy
                      self.variable_singular = @model.find(params[:id])
                      authorize self.variable_singular
                      
                      unless self.variable_singular.destroy
                        raise Errors::DestroyFailedError, "destroy failed for #{self.variable_singular.id} #{self.variable_singular.class.name}"
                      end
                      
                      render :nothing => true, :status => 204
                    end


                    def mass_destroy
                      policy_scope(@model).where(id: params[:mass_destroy_ids]).each do |record|
                        authorize record 

                        unless record.destroy
                          raise Errors::DestroyFailedError, "destroy failed for #{record.id} #{record.class.name}"
                        end
                      end

                      render :nothing => true, :status => 204
                    end
                  end  
                end
              end
            end
          end
        end
      end
    end
  end
end