

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Actions
              module Update
                def self.included(base)
                
                  base.class_eval do
                    def update
                      raise Errors::InvalidModelError, "Please define #{@model.name}::API_UPDATE_ATTRS" unless @model.constants.include? :API_UPDATE_ATTRS
                      self.variable_singular = @model.find(params[:id])
                      self.variable_singular = modify_on_update(self.variable_singular)

                      validate self.variable_singular
                      authorize self.variable_singular
                      
                      unless self.variable_singular.update(params.require(@variable_params).permit(*(@model.const_get(:API_UPDATE_ATTRS))))
                        raise Errors::UpdateFailedError, "update failed for #{self.variable_singular.id} #{self.variable_singular.class.name}"
                      end

                      render_default_show({ status: :created })
                    end


                    def modify_on_update(record)
                      record
                    end
                  end  
                end
              end
            end
          end
        end
      end
    end
  end
end