

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Actions
              module Index
                INTEGER_REGEXP = /\A-?[0-9]+\z/.freeze
                FLOAT_REGEXP   = /\A-?[0-9]+(\.[0-9]+)?\z/.freeze
                DECIMAL_REGEXP = /\A-?[0-9]+(\.[0-9]+)?\z/.freeze
                DATE_REGEXP    = /\A[12][0-9]{3}-[01][0-9]-[0-3][0-9]\z/.freeze
                TIME_REGEXP    = /\A[012][0-9]:[0-5][0-9]:[0-5][0-9]\z/.freeze

                def self.included(base)
                  base.class_eval do
                    def index
                      self.variable_plural = policy_scope(@model)
                      index_extract_conditions

                      respond_to { |format| format.json { render template: "radio_kit/foundation/rails/api/rest/index" }}
                    end


                    protected

                    def index_extract_conditions
                      if params.has_key? :a
                        raise Errors::InvalidModelError, "Please define #{@model.name}::API_INDEX_ATTRS" unless @model.constants.include? :API_INDEX_ATTRS
                        raise Errors::InvalidConditionsError, "attributes parameter was specified but it is not an array" unless params[:a].is_a? Array

                        attrs_diff = params[:a].map(&:to_sym) - @model.const_get(:API_INDEX_ATTRS)
                        raise Errors::InvalidConditionsError, "attributes parameter was specified but it contains disallowed attributes #{attrs_diff.inspect}" unless attrs_diff.empty?

                        @attrs = (["id"] + params[:a]).uniq
                        collection_without_select = self.variable_plural.dup
                        self.variable_plural = self.variable_plural.select((@attrs & @model.columns_hash.keys).map{|x| "#{@model.table_name}.#{x}" })

                      else
                        raise Errors::InvalidConditionsError, "attributes parameter was not specified"
                      end

                      if params.has_key? :c
                        raise Errors::InvalidModelError, "Please define #{@model.name}::API_INDEX_CONDITIONS" unless @model.constants.include? :API_INDEX_CONDITIONS
                        raise Errors::InvalidConditionsError, "conditions parameter was specified but it is not an associative array" and return unless params[:c].is_a? Hash

                        params[:c].each do |parameter, parameter_conditions|
                          raise Errors::InvalidConditionsError, "conditions parameter was specified but its key for parameter \"#{parameter}\" does not contain an array of conditions" and return false unless params[:c][parameter].is_a? Array

                          parameter_conditions.each do |parameter_condition|
                            raise Errors::InvalidConditionsError, "conditions parameter was specified but its key for parameter \"#{parameter}\" does not contain valid value" and return false unless parameter_condition.is_a? String

                            parameter_condition_parts     = parameter_condition.split(" ", 2)
                            parameter_conditions_operator = parameter_condition_parts.first
                            parameter_conditions_value    = parameter_condition_parts.last

                            parameter_parts = parameter.split(".")

                            if parameter_parts.size == 1
                              model = @model
                              parameter_stripped = parameter
                              raise Errors::InvalidConditionsError, "conditions parameter was specified but its key for parameter \"#{parameter}\" cannot be used in conditions clause for this collection" and return false unless model.const_get(:API_INDEX_CONDITIONS).include? parameter.to_sym

                            elsif parameter_parts.size == 2
                              raise Errors::InvalidConditionsError, "conditions parameter was specified but its key for parameter \"#{parameter}\" refers to table \"#{parameter_parts.first}\" that is not allowed to be joined so it cannot be used in conditions clause for this collection" and return false unless @model.const_get(:API_JOINS).include? parameter_parts.first.to_sym
                              model = parameter_parts.first.camelize.singularize.constantize
                              parameter_stripped = parameter_parts.last
                              raise Errors::InvalidConditionsError, "conditions parameter was specified but its key for parameter \"#{parameter}\" refers to column \"#{parameter_stripped}\" of joined table \"#{parameter_parts.first}\" but it cannot be used in conditions clause for this collection" and return false unless model.const_get(:API_INDEX_CONDITIONS).include? parameter_stripped.to_sym
                              self.variable_plural = self.variable_plural.joins(parameter_parts.first.to_sym)

                            else
                              raise Errors::InvalidConditionsError, "conditions parameter was specified but has invalid syntax" and return false
                            end

                            # Convert operators
                            sql_operator = case parameter_conditions_operator
                              when "in"      then "IN (?)"
                              when "eq"      then "= ?"
                              when "lt"      then "< ?"
                              when "gt"      then "> ?"
                              when "lte"     then "<= ?"
                              when "gte"     then ">= ?"
                              when "not"     then "<> ?"
                              when "isnull"  then "IS NULL"
                              when "notnull" then "IS NOT NULL"
                              else raise Errors::InvalidConditionsError, "conditions parameter was specified but condition \"#{parameter_condition}\" for parameter \"#{parameter}\" has unknown operator" and return false
                            end

                            if parameter_conditions_operator == "in"
                              parameter_conditions_value_parsed = parameter_conditions_value.split("|").map{|x| parameter_value_parse(model, parameter_stripped, x) }
                            elsif parameter_conditions_operator == "isnull" or parameter_conditions_operator == "notnull"
                              # noop, no conditions value
                            else
                              parameter_conditions_value_parsed = parameter_value_parse(model, parameter_stripped, parameter_conditions_value) # TODO add support for enums in "in"
                            end

                            # Test parameter/column type combination
                            raise Errors::InvalidConditionsError, "conditions parameter was specified but condition \"#{parameter_condition}\" for parameter \"#{parameter}\" uses operator #{parameter_conditions_operator} that can be used only with numeric or date/time attributes" and return false if ["lt", "gt", "lte", "gte"].include?(parameter_conditions_operator) and not [ :integer, :float, :decimal, :date, :datetime, :time, :timestamp ].include?(model.columns_hash[parameter_stripped].type)

                            if sql_operator.include? "?"
                              # Apply conditions also to collection_without_select. May be needed later for count total
                              collection_without_select = collection_without_select.where(["#{model.table_name}.#{parameter_stripped} #{sql_operator}", parameter_conditions_value_parsed])
                              self.variable_plural = self.variable_plural.where(["#{model.table_name}.#{parameter_stripped} #{sql_operator}", parameter_conditions_value_parsed])
                            else
                              collection_without_select = collection_without_select.where("#{model.table_name}.#{parameter_stripped} #{sql_operator}")
                              self.variable_plural = self.variable_plural.where("#{model.table_name}.#{parameter_stripped} #{sql_operator}")
                            end
                          end
                        end
                      end



                      if params.has_key? :s
                        raise Errors::InvalidModelError, "Please define #{@model.name}::API_SCOPES" unless @model.constants.include? :API_SCOPES
                        raise Errors::InvalidConditionsError, "scopes parameter was specified but it is not an array" and return unless params[:s].is_a? Hash

                        params[:s].each do |parameter_scope, parameter_arguments|
                          raise Errors::InvalidConditionsError, "scope parameter was specified but its key for parameter \"#{parameter_scope}\" is not allowed to be scoped" and return false unless @model.const_get(:API_SCOPES).include? parameter_scope.to_sym

                          if parameter_arguments == ""
                            self.variable_plural = self.variable_plural.send(parameter_scope)

                          elsif parameter_arguments.is_a? Array
                            self.variable_plural = self.variable_plural.send(parameter_scope, *parameter_arguments)

                          else
                            raise Errors::InvalidConditionsError, "scope parameter was specified but its value is not an array of scope arguments" and return false

                          end

                          raise Errors::InvalidConditionsError, "scope parameter was specified but scope \"#{parameter}\" does not return ActiveRecord::Relation" and return false unless self.variable_plural.is_a? ActiveRecord::Relation
                        end
                      end

                      if params.has_key? :ct
                        @count_total = collection_without_select.count
                      end


                      if params.has_key? :j
                        raise Errors::InvalidModelError, "Please define #{@model.name}::API_JOINS" unless @model.constants.include? :API_JOINS
                        raise Errors::InvalidConditionsError, "joins parameter was specified but it is not an array" and return unless params[:j].is_a? Array

                        params[:j].uniq.each do |parameter|
                          raise Errors::InvalidConditionsError, "join parameter was specified but its key for parameter \"#{parameter}\" is not allowed to be joined" and return false unless @model.const_get(:API_JOINS).include? parameter.to_sym

                          self.variable_plural = self.variable_plural.includes(parameter.to_sym)
                        end
                      end


                      if params.has_key? :l
                        raise Errors::InvalidConditionsError, "limit parameter was specified but it is not a string" and return unless params[:l].is_a? String
                        raise Errors::InvalidConditionsError, "limit parameter was specified but it has invalid syntax" and return unless params[:l] =~ /\A([0-9]+),([1-9][0-9]*)\z/

                        self.variable_plural = self.variable_plural.offset($1.to_i).limit($2.to_i)
                      end


                      if params.has_key? :o
                        raise Errors::InvalidConditionsError, "order parameter was specified but it is not a string" and return unless params[:o].is_a? String
                        raise Errors::InvalidConditionsError, "order parameter was specified but it has invalid syntax" and return unless params[:o] =~ /\A([a-z0-9\_\.]+),(asc|desc)\z/
                        raise Errors::InvalidConditionsError, "order parameter was specified but specified \"#{$1}\" cannot be used in order clause for this collection" and return false unless @model.const_get(:API_INDEX_CONDITIONS).include? $1.to_sym

                        if $1.include?(".")
                          self.variable_plural = self.variable_plural.order("#{$1.split('.')[0]} -> LOWER('#{$1.split('.')[1]}') #{$2}")
                        else
                          self.variable_plural = self.variable_plural.order($1.to_sym => $2.to_sym)
                        end
                      end

                      true
                    end


                    def parameter_value_parse(model, parameter_stripped, value)
                      # TODO add regexps for strict testing syntaxes

                      column = model.columns_hash[parameter_stripped]

                      case column.type
                        when :integer   then raise Errors::InvalidConditionsError, "parameter \"#{parameter_stripped}\" has invalid syntax, it must be an integer" unless value.to_s.match(INTEGER_REGEXP) unless model.defined_enums.has_key? parameter_stripped
                        when :float     then raise Errors::InvalidConditionsError, "parameter \"#{parameter_stripped}\" has invalid syntax, it must be a float with optional fractional part separated by a dot" unless value.to_s.match(FLOAT_REGEXP)
                        when :decimal   then raise Errors::InvalidConditionsError, "parameter \"#{parameter_stripped}\" has invalid syntax, it must be a decimal with optional fractional part separated by a dot" unless value.to_s.match(DECIMAL_REGEXP)
                        when :date      then raise Errors::InvalidConditionsError, "parameter \"#{parameter_stripped}\" has invalid syntax, it must be a date in format YYYY-MM-DD" unless value.to_s.match(DATE_REGEXP)
                        # when :datetime  then Time.zone.parse(value) # TODO
                        # when :timestamp then Time.zone.parse(value) # TODO
                        when :time      then raise Errors::InvalidConditionsError, "parameter \"#{parameter_stripped}\" has invalid syntax, it must be a time in format HH:MM:SS" unless value.to_s.match(TIME_REGEXP)
                        else value
                      end

                      if column.nil?
                        raise Errors::InvalidConditionsError, "parameter \"#{parameter_stripped}\" has invalid value, it must be one of #{model.defined_enums[parameter_stripped].keys.join(", ")} but \"#{value}\" was given" unless model.defined_enums[parameter_stripped].has_key? value
                      end

                      if model.defined_enums.has_key? parameter_stripped
                        raise Errors::InvalidConditionsError, "parameter \"#{parameter_stripped}\" has invalid value, it must be one of #{model.defined_enums[parameter_stripped].keys.join(", ")} but \"#{value}\" was given" unless model.defined_enums[parameter_stripped].has_key? value
                        value_parsed = model.defined_enums[parameter_stripped][value]

                      else
                        value_parsed = case column.type
                          when :integer   then value.to_i
                          when :float     then value.to_f
                          when :decimal   then BigDecimal.new(value)
                          when :date      then Date.parse(value)
                          when :datetime  then Time.zone.parse(value)
                          when :timestamp then Time.zone.parse(value)
                          when :time      then Time.parse(value)
                          else value
                        end
                      end

                      value_parsed
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end