



module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Actions
              module Common
                def self.included(base)
                
                  base.class_eval do
                    def render_default_show(params = {})
                      respond_to { |format| format.json { render params.merge({template: "radio_kit/foundation/rails/api/rest/show" }) } }
                    end


                    def validate(record)
                      unless record.valid?
                        if record.errors[:overlap] and record.errors[:overlap].any?
                          raise Errors::OverlappingError, record.errors.collect{ |k,v| "#{v}" }.join(", ") 
                        else
                          raise Errors::InvalidRecordError, record.errors.collect{ |k,v| "#{k}: #{v}" }.join(", ")
                        end
                      end
                    end
                  end  
                end
              end
            end
          end
        end
      end
    end
  end
end