

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Actions
              module Show
                def self.included(base)
                
                  base.class_eval do
                    def show
                      raise Errors::InvalidModelError, "Please define #{@model.name}::API_INDEX_ATTRS" unless @model.constants.include? :API_INDEX_ATTRS

                      if params.has_key? :a
                        raise Errors::InvalidModelError, "Please define #{@model.name}::API_INDEX_ATTRS" unless @model.constants.include? :API_INDEX_ATTRS
                        raise Errors::InvalidConditionsError, "attributes parameter was specified but it is not an array" unless params[:a].is_a? Array

                        attrs_diff = params[:a].map(&:to_sym) - @model.const_get(:API_INDEX_ATTRS)
                        raise Errors::InvalidConditionsError, "attributes parameter was specified but it contains disallowed attributes #{attrs_diff.inspect}" unless attrs_diff.empty? 

                        @attrs = (["id"] + params[:a]).uniq

                        if params.has_key? :alias 
                          if @model.columns_hash.has_key? "alias"
                            self.variable_singular = @model.select(@attrs & @model.columns_hash.keys).find_by!(alias: params[:alias])
                          else
                            raise Errors::InvalidModelError, "Trying to find record by alias but that model has no alias attribute"
                          end

                        elsif params.has_key? :id
                          self.variable_singular = @model.select(@attrs & @model.columns_hash.keys).find(params[:id])
                        end

                      else
                        raise Errors::InvalidConditionsError, "attributes parameter was not specified"
                      end

                      authorize self.variable_singular
                      render_default_show
                    end
                  end  
                end
              end
            end
          end
        end
      end
    end
  end
end