

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Actions
              module Create
                def self.included(base)
                
                  base.class_eval do
                    def create
                      raise Errors::InvalidModelError, "Please define #{@model.name}::API_CREATE_ATTRS" unless @model.constants.include? :API_CREATE_ATTRS
                      self.variable_singular = @model.new(params.require(@variable_params).permit(*(@model.const_get(:API_CREATE_ATTRS))))
                      self.variable_singular = modify_on_create(self.variable_singular)

                      validate self.variable_singular
                      authorize self.variable_singular

                      unless self.variable_singular.save
                        raise Errors::CreateFailedError, "create failed for #{self.variable_singular.id} #{self.variable_singular.class.name}"
                      end

                      render_default_show({ status: :created })
                    end
                    

                    def mass_create
                      params.require(:mass_create_params).each do |k, v|
                        record = @model.new(ActionController::Parameters.new(v).require(@variable_params).permit(*(@model.const_get(:API_CREATE_ATTRS))))
                        record = modify_on_create(record)

                        validate record
                        authorize record
                        record.save!
                      end

                      render :nothing => true, :status => :created
                    end


                    def modify_on_create(record)
                      record
                    end
                  end  
                end
              end
            end
          end
        end
      end
    end
  end
end