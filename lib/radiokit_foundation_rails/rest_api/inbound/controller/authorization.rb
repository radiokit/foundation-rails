

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Authorization
              def self.included(base)
              
                base.class_eval do
                  include ::Pundit

                  after_action  :verify_authorized, except: :index
                  after_action  :verify_policy_scoped, only: :index
                end  
              end
            end
          end
        end
      end
    end
  end
end