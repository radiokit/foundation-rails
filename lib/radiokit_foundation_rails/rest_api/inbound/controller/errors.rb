

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Errors
              class InvalidModelError < RuntimeError; end
              class InvalidRecordError < RuntimeError; end
              class InvalidConditionsError < RuntimeError; end
              class CreateFailedError < RuntimeError
                attr_reader :record
                def initialize(message, record)
                  super(message)
                  @record = record
                end
              end
              class DestroyFailedError < RuntimeError
                attr_reader :record
                def initialize(message, record)
                  super(message)
                  @record = record
                end
              end
              class UpdateFailedError < RuntimeError
                attr_reader :record
                def initialize(message, record)
                  super(message)
                  @record = record
                end
              end
              class OverlappingError < RuntimeError; end
              class AuthorizationFailedError < RuntimeError; end
              class AuthorizationInternalError < RuntimeError; end


              def self.included(base)
                base.class_eval do
                  rescue_from Pundit::NotAuthorizedError,         with: :render_error_forbidden
                  rescue_from ActiveRecord::RecordNotFound,       with: :render_error_not_found
                  rescue_from ActiveRecord::RecordNotUnique,      with: :render_error_not_unique
                  rescue_from InvalidRecordError,                 with: :render_error_invalid_parameters_child_params
                  rescue_from InvalidConditionsError,             with: :render_error_invalid_parameters_conditions
                  rescue_from ActionController::ParameterMissing, with: :render_error_invalid_parameters_root_param
                  rescue_from ArgumentError,                      with: :render_error_invalid_parameters_enum
                  rescue_from CreateFailedError,                  with: :render_error_create_failed
                  rescue_from UpdateFailedError,                  with: :render_error_update_failed
                  rescue_from DestroyFailedError,                 with: :render_error_destroy_failed
                  rescue_from OverlappingError,                   with: :render_error_overlapping
                  rescue_from AuthorizationFailedError,           with: :render_error_unauthorized
                  rescue_from AuthorizationInternalError,         with: :render_error_internal_error


                  protected


                  def render_error_not_found
                    render json: { error: "not-found" }, status: :not_found
                  end

                  
                  def render_error_forbidden
                    render json: { error: "forbidden", info: $!.message }, status: :forbidden
                  end


                  def render_error_unauthorized
                    render json: { error: "unauthorized", info: $!.message }, status: :unauthorized
                  end


                  def render_error_invalid_parameters_child_params
                    render json: { error: "invalid-parameters", info: $!.message }, status: :unprocessable_entity
                  end


                  def render_error_invalid_parameters_root_param
                    render json: { error: "invalid-parameters", info: $!.message }, status: :unprocessable_entity
                  end


                  def render_error_invalid_parameters_enum
                    render json: { error: "invalid-parameters", info: $!.message }, status: :unprocessable_entity
                  end


                  def render_error_invalid_parameters_conditions
                    render json: { error: "invalid-conditions", info: $!.message }, status: :unprocessable_entity
                  end


                  def render_error_create_failed
                    render json: { error: "create-failed", reason: $!.record.errors, info: $!.message }, status: :unprocessable_entity
                  end  


                  def render_error_update_failed
                    render json: { error: "update-failed", reason: $!.record.errors, info: $!.message }, status: :unprocessable_entity
                  end  


                  def render_error_destroy_failed
                    render json: { error: "destroy-failed", reason: $!.record.errors, info: $!.message }, status: :unprocessable_entity
                  end


                  def render_error_overlapping
                    render json: { error: "overlapping", info: $!.message }, status: :conflict
                  end


                  def render_error_not_unique
                    render json: { error: "not_unique", info: $!.message }, status: :conflict
                  end


                  def render_error_internal_error
                    render json: { error: "internal-error", info: $!.message }, status: :internal_error
                  end
                end  
              end
            end
          end
        end
      end
    end
  end
end