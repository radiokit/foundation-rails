

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Authentication
              def self.included(base)
                base.class_eval do
                  # before_action :authenticate_with_radiokit!

                  protected


                  def current_user
                    @current_user
                  end


                  def sign_in!(user)
                    @current_user = user
                  end


                  def access_token
                    if request.headers["Authorization"]
                      if request.headers["Authorization"].start_with? "Bearer "
                        request.headers["Authorization"].split(" ", 2).last
                      end
                    end
                  end


                  def authenticate_with_radiokit!
                    if access_token
                      # TODO cache this

                      res = InternalApi::Call.get "radiokit-auth", "/access_tokens/confirm", { access_token: access_token }

                      case res.status
                      when 200
                        ActiveRecord::Base.transaction do
                          data = MultiJson.load(res.body)["ok"]["data"]

                          data.each do |data_class_name, data_records|
                            data_class = nil

                            begin
                              data_class = data_class_name.constantize
                            rescue NameError => e
                              fail "Please define #{data_class_name} class that is compatible with ActiveModel"
                            end

                            data_records.each do |data_record|
                              if data_class.where(id: data_record["id"]).exists?
                                db_record = data_class.where(id: data_record["id"]).first

                              else
                                db_record = data_class.new
                              end

                              data_record.keys.each do |key|
                                db_record.send "#{key}=", data_record[key] if db_record.respond_to? "#{key}="
                              end

                              db_record.save!
                            end
                          end

                          owner_info = MultiJson.load(res.body)["ok"]["owner"]
                          owner_class    = nil
                          owner_instance = nil

                          begin
                            owner_class = owner_info["class_name"].constantize
                          rescue NameError => e
                            fail "Please define #{owner_info["class_name"]} class that is compatible with ActiveModel"
                          end

                          owner_instance = owner_class.find_by_id(owner_info["id"]) || owner_class.create!(id: owner_info["id"]) # TODO create also other attributes, not only ID

                          sign_in! owner_instance
                        end

                      when 403
                        raise Errors::AuthorizationFailedError, "Invalid token"

                      else
                        raise Errors::AuthorizationInternalError, "Unable to comunicate with the auth server"
                      end

                    else
                      raise Errors::AuthorizationFailedError, "Missing Authorization header"
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
