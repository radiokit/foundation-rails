

module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            module Common
              def self.included(base)
                base.class_eval do
                  skip_before_action :verify_authenticity_token
                  
                  before_action :append_common_response_headers
                  before_action :initialize_common_variables
                  around_action :wrap_in_transaction

                  protected

                  def append_common_response_headers
                    response.headers["Vary"] = "Accept,Accept-Encoding,Authorization"
                  end


                  def initialize_common_variables
                    
                    @controller             = params[:controller].split("/").last
                    @model_name             = params[:controller].split("/")[2..-1].join("/").classify
                    @model                  = @model_name.constantize
                    @variable_params        = :"#{@controller.singularize}"
                    @variable_singular_name = "@#{@controller.singularize}"
                    @variable_plural_name   = "@#{@controller.pluralize}"
                  end


                  def variable_singular
                    instance_variable_get(@variable_singular_name)
                  end


                  def variable_singular=(v)
                    instance_variable_set(@variable_singular_name, v)
                  end


                  def variable_plural
                    instance_variable_get(@variable_plural_name)
                  end


                  def variable_plural=(v)
                    instance_variable_set(@variable_plural_name, v)
                  end  


                  def wrap_in_transaction(&b)
                    ActiveRecord::Base.transaction do
                      yield
                    end
                  end
                end  
              end
            end
          end
        end
      end
    end
  end
end