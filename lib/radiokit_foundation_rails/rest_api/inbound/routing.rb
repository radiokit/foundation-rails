module ActionDispatch::Routing
  class Mapper
    def radiokit_rest_api(&b)
      namespace :api do
        namespace :rest do
          yield if block_given?

          # TODO check if model name makes sense and is singular
          get    "*model/:id"    => "generic#show",    format: false, id: /[0-9]+/
          get    "*model/:id"    => "generic#show",    format: false, id: /[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}/
          get    "*model/:alias" => "generic#show",    format: false, alias: /[a-zA-Z0-9\.]+/
          get    "*model"        => "generic#index",   format: false
          patch  "*model/:id"    => "generic#update",  format: false, id: /[0-9]+/
          patch  "*model/:id"    => "generic#update",  format: false, id: /[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}/
          patch  "*model/:alias" => "generic#update",  format: false, alias: /[a-zA-Z0-9\.]+/
          post   "*model"        => "generic#create",  format: false
          delete "*model"        => "generic#destroy", format: false
        end
      end
    end
  end
end

