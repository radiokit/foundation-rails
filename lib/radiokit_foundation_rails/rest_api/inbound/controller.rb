
module RadioKit
  module Foundation
    module Rails
      module RestApi
        module Inbound
          module Controller
            def self.included(base)
              base.class_eval do
                include Common
                include Errors
                include Actions::Common
                include Actions::Index
                include Actions::Show
                include Actions::Update
                include Actions::Create
                include Actions::Destroy
                include Authentication
                include Authorization
              end
            end
          end
        end
      end
    end
  end
end