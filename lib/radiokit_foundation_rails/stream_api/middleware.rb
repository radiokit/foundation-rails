module RadioKit
  module Foundation
    module Rails
      module StreamApi
        class Middleware
          KEEPALIVE_TIME = 15 # in seconds
          
          def initialize(app)
            @app = app
            @clients = []

            RadioKit::Foundation::Rails::StreamApi::Call.subscribe("/broadcast") do |channel, message|
              @clients.each do |client|
                client.send message
              end
            end
          end


          def call(env)
            if Faye::WebSocket.websocket?(env)
              ws = Faye::WebSocket.new(env, nil, { ping: KEEPALIVE_TIME })
              ws.on :open do |event|
                p [:open, ws.object_id]
                @clients << ws
              end

              ws.on :message do |event|
                p [:message, event.data]
                # @redis.publish(CHANNEL, sanitize(event.data))
              end

              ws.on :close do |event|
                p [:close, ws.object_id, event.code, event.reason]
                @clients.delete(ws)
                ws = nil
              end

              # Return async Rack response
              ws.rack_response

            else

              @app.call(env)
            end
          end
        end
      end
    end
  end
end