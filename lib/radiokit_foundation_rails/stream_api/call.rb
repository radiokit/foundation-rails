module RadioKit
  module Foundation
    module Rails
      module StreamApi
        class Call
          def self.raw_publish(channel, message)
            StreamApi.redis.publish channel, message
          end


          def self.broadcast(message)
            raw_publish "/broadcast", { timestamp: Time.now, data: message }.to_json
          end


          def self.subscribe(channel, &b)
            Thread.new do
              StreamApi.redis.subscribe(channel) do |on|
                on.message do |channel, message|
                  yield channel, message
                end
              end
            end
          end
        end
      end
    end
  end
end