module RadioKit
  module Foundation
    module Rails
      module StreamApi
        def self.redis
          if Thread.current[:radiokit_foundation_rails_stream_api_redis].nil?
            url = ENV["REDIS_STREAM_API_URL"] || DEFAULT_REDIS_STREAM_API_URL

            Logger.info "[StreamApi] Connecting to redis (#{url})" 
            
            Thread.current[:radiokit_foundation_rails_stream_api_redis] = Redis.new(url: url, driver: :hiredis)
          end

          Thread.current[:radiokit_foundation_rails_stream_api_redis]
        end
      end
    end
  end
end