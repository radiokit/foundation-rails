require 'pundit'
require 'jbuilder'
require 'faraday'
require 'newrelic_rpm'
require 'lograge'
require 'rack/cors'
require 'oj'
require 'multi_json'
require 'redis'
require 'faye/websocket'
require 'radiokit_foundation_rails/engine'
require 'radiokit_foundation_rails/internal_api'
require 'radiokit_foundation_rails/internal_api/call'
require 'radiokit_foundation_rails/internal_api/inbound/authentication'
require 'radiokit_foundation_rails/internal_api/inbound/authentication/constraint'
require 'radiokit_foundation_rails/internal_api/inbound/authentication/routing'
require 'radiokit_foundation_rails/internal_api/inbound/authentication/controller'
require 'radiokit_foundation_rails/stream_api'
require 'radiokit_foundation_rails/stream_api/middleware'
require 'radiokit_foundation_rails/stream_api/call'
require 'radiokit_foundation_rails/rest_api/inbound/routing'
require 'radiokit_foundation_rails/rest_api/inbound/controller/common'
require 'radiokit_foundation_rails/rest_api/inbound/controller/errors'
require 'radiokit_foundation_rails/rest_api/inbound/controller/actions/common'
require 'radiokit_foundation_rails/rest_api/inbound/controller/actions/index'
require 'radiokit_foundation_rails/rest_api/inbound/controller/actions/show'
require 'radiokit_foundation_rails/rest_api/inbound/controller/actions/update'
require 'radiokit_foundation_rails/rest_api/inbound/controller/actions/create'
require 'radiokit_foundation_rails/rest_api/inbound/controller/actions/destroy'
require 'radiokit_foundation_rails/rest_api/inbound/controller/authentication'
require 'radiokit_foundation_rails/rest_api/inbound/controller/authorization'
require 'radiokit_foundation_rails/rest_api/inbound/controller'

module RadioKit
  module Foundation
    module Rails
      DEFAULT_REDIS_STREAM_API_URL = "redis://127.0.0.1:6379/1"


      module Logger
        def self.info(message)
          if defined?(::Rails) and ::Rails.logger
            ::Rails.logger.info message
          else
            puts message
          end
        end


        def self.warn(message)
          if defined?(::Rails) and ::Rails.logger
            ::Rails.logger.warn message
          else
            $stderr.puts message
          end
        end
      end
    end
  end
end
